﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace IoTMQTT.Models {
    public class Temperature {
        [Key]
        public int Id { get; set; }

        [Required]
        public double Value { get; set; }

        public DateTime InsertedAt { get; set; }
    }
}

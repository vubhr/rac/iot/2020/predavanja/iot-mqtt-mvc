﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IoTMQTT.Data;
using IoTMQTT.Models;

namespace IoTMQTT.Controllers
{
    public class MotionsController : Controller
    {
        private readonly IoTMQTTDbContext _context;

        public MotionsController(IoTMQTTDbContext context)
        {
            _context = context;
        }

        // GET: Motions
        public async Task<IActionResult> Index()
        {
            return View(await _context.Motions.ToListAsync());
        }

        // GET: Motions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var motion = await _context.Motions
                .FirstOrDefaultAsync(m => m.Id == id);
            if (motion == null)
            {
                return NotFound();
            }

            return View(motion);
        }

        // GET: Motions/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Motions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Value,InsertedAt")] Motion motion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(motion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(motion);
        }

        // GET: Motions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var motion = await _context.Motions.FindAsync(id);
            if (motion == null)
            {
                return NotFound();
            }
            return View(motion);
        }

        // POST: Motions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Value,InsertedAt")] Motion motion)
        {
            if (id != motion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(motion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MotionExists(motion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(motion);
        }

        // GET: Motions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var motion = await _context.Motions
                .FirstOrDefaultAsync(m => m.Id == id);
            if (motion == null)
            {
                return NotFound();
            }

            return View(motion);
        }

        // POST: Motions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var motion = await _context.Motions.FindAsync(id);
            _context.Motions.Remove(motion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MotionExists(int id)
        {
            return _context.Motions.Any(e => e.Id == id);
        }
    }
}

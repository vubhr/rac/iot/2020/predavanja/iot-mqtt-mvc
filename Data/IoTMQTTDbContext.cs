﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using IoTMQTT.Models;

namespace IoTMQTT.Data
{
    public class IoTMQTTDbContext : DbContext
    {
        public IoTMQTTDbContext (DbContextOptions<IoTMQTTDbContext> options)
            : base(options)
        {
        }

        public DbSet<IoTMQTT.Models.Temperature> Temperatures { get; set; }

        public DbSet<IoTMQTT.Models.Motion> Motions { get; set; }
    }
}
